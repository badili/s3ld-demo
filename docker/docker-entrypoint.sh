#!/bin/bash

cd /opt/s3ld

## run migrations
python manage.py migrate --noinput --run-syncdb

## collect statics
python manage.py collectstatic --noinput

## create superuser
#python manage.py createsuperuser

python manage.py shell << EOF
from os import environ
from django.contrib.auth.models import User
username=environ.get('DJANGO_ADMIN_USERNAME')
password=environ.get('DJANGO_ADMIN_PASSWORD')
email=environ.get('DJANGO_ADMIN_EMAIL')
User.objects.filter(email=email).delete()
User.objects.create_superuser(username, email, password)
EOF

## runserver
python manage.py runserver 0.0.0.0:8000