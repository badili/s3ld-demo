function BadiliDash() {
    this.data = {};
    this.theme = '';        // for jqWidgets
    this.console = console;
    this.currentView = undefined;
    this.csrftoken = $('meta[name=csrf-token]').attr('content');
    this.reEscape = new RegExp('(\\' + ['/', '.', '*', '+', '?', '|', '(', ')', '[', ']', '{', '}', '\\'].join('|\\') + ')', 'g')
    this.d3_width = 900;
    this.d3_height = 300;

    $.ajaxSetup({
      beforeSend: function(xhr, settings) {
        if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type)) {
            xhr.setRequestHeader("X-CSRFToken", dash.csrftoken)
        }
      }
    });

    this.backgroundColor1 = [
        'rgba(255, 99,  132, 0.2)',
        'rgba(54,  162, 235, 0.2)',
        'rgba(255, 206, 86,  0.2)',
        'rgba(75,  192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64,  0.2)'
    ];

    this.backgroundColor2 = [
        'rgba(255,128,128, 0.8)',
        'rgba(255,156,128, 0.8)',
        'rgba(255,184,128, 0.8)',
        'rgba(255,212,128, 0.8)',
        'rgba(255,241,128, 0.8)',
        'rgba(241,255,128, 0.8)',
        'rgba(213,255,128, 0.8)',
        'rgba(184,255,128, 0.8)',
        'rgba(156,255,128, 0.8)',
        'rgba(128,255,128, 0.8)',
        'rgba(128,255,156, 0.8)',
        'rgba(128,255,184, 0.8)',
        'rgba(128,255,212, 0.8)',
        'rgba(128,255,241, 0.8)',
        'rgba(128,241,255, 0.8)',
        'rgba(128,212,255, 0.8)',
        'rgba(128,184,255, 0.8)',
        'rgba(128,156,255, 0.8)',
        'rgba(128,128,255, 0.8)',
        'rgba(156,128,255, 0.8)',
        'rgba(184,128,255, 0.8)',
        'rgba(212,128,255, 0.8)',
        'rgba(241,128,255, 0.8)',
        'rgba(255,128,241, 0.8)',
        'rgba(255,128,213, 0.8)',
        'rgba(255,128,184, 0.8)',
        'rgba(255,128,156, 0.8)',
    ];

    this.bgColors = [[
            'rgba(255, 99,  132, 0.2)',
            'rgba(54,  162, 235, 0.2)',
            'rgba(255, 206, 86,  0.2)',
            'rgba(75,  192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64,  0.2)'
        ],
        [
            '#ff8080', '#ff9c80', '#ffb880', '#ffd480', '#fff180', '#f1ff80', '#d5ff80', '#b8ff80', '#9cff80','#80ff80',
            '#80ff9c', '#80ffb8', '#80ffd4', '#80fff1', '#80f1ff', '#80d4ff', '#80b8ff', '#809cff', '#8080ff', '#9c80ff',
            '#b880ff', '#d480ff', '#f180ff', '#ff80f1', '#ff80d5', '#ff80b8', '#ff809c',
        ]
    ];

    this.default_template = "<div class=\"col-lg-6\">\n\
        <div class=\"ibox float-e-marg\">\n\
            <div class=\"ibox-title\">\n\
                <h5>%s</h5>\n\
            </div>\n\
            <div class=\"ibox-content\">\n\
                <div>\n\
                    <canvas id='%s' height='200'></canvas>\n\
                </div>\n\
            </div>\n\
        </div>\n\
    </div>";

    this.liv_keeping_template = "<div class=\"col-lg-6\">\n\
        <div class=\"ibox float-e-marg\">\n\
            <div class=\"ibox-title\">\n\
                <h5>\n\
                    %s\n\
                    <small>Primary vs Secondary Reasons</small>\n\
                </h5>\n\
            </div>\n\
            <div class=\"ibox-content\">\n\
                <div>\n\
                    <canvas id='%s' height='200'></canvas>\n\
                </div>\n\
            </div>\n\
        </div>\n\
    </div>";
    var merged_views_end = "</div>";

    $('#all_forms').on('select', function(event){
        var args = event.args;
        if (args) {
            // get the form structure
            dash.formStructure(args.item.originalItem.uid);
        }
    });
    $(document).on('click', '.reporting_period', this.changeReportingPeriod);
}

BadiliDash.prototype.initiate = function(){
    // initiate the creation of the interface
    this.initiateAllForms();
    this.initiateButtonsRadios();
};

/**
 * Creates a combobox with a list of all the forms
 * @returns {undefined}
 */
BadiliDash.prototype.initiateAllForms = function(){
    console.log(dash.data.all_forms);
    var source = {
        localdata: dash.data.all_forms,
        id:"id",
        datatype: "json",
        datafields:[ {name:"id"}, {name:"title"} ]
    };
    var data_source = new $.jqx.dataAdapter(source);
    $("#all_forms").jqxComboBox({ selectedIndex: 0, source: data_source, displayMember: "title", valueMember: "id", width: '97%', theme: dash.theme });
    $("#all_forms").addClass('form-control m-b');
};


BadiliDash.prototype.formStructure = function (form_id) {
    var data = {'form_id': form_id};

    $('#spinnermModal').modal('show');
    $.ajax({
        type: "POST", url: "/form_structure/", dataType: 'json', data: data,
        error: dash.communicationError,
        success: function (data) {
            $('#spinnermModal').modal('hide');
            if (data.error) {
                console.log(data.message);
                swal({
                  title: "Error!",
                  text: data.message,
                  imageUrl: "/static/img/error-icon.png"
                });
                return;
            } else {
                // console.log(data);
                dash.curFormStructure = data.structure;
                dash.initiateFormStructureTree();
            }
        }
    });
};


BadiliDash.prototype.initiateFormStructureTree = function () {
    var source ={
        datatype: "json",
        datafields: [
            { name: 'id', type: 'number' },
            { name: 'parent_id', type: 'number' },
            { name: 'name', type: 'string' },
            { name: 'type', type: 'string' },
            { name: 'label', type: 'string' }
        ],
        hierarchy:{
            keyDataField: { name: 'id' },
            parentDataField: { name: 'parent_id' }
        },
        id: 'id',
        localdata: dash.curFormStructure
    };

    // create data adapter.
    var dataAdapter = new $.jqx.dataAdapter(source);

    /**
    // perform Data Binding.
    dataAdapter.dataBind();
    // get the tree items. The first parameter is the item's id. The second parameter is the parent item's id. The 'items' parameter represents
    // the sub items collection name. Each jqxTree item has a 'label' property, but in the JSON data, we have a 'text' field. The last parameter
    // specifies the mapping between the 'text' and 'label' fields.
    console.log(dash.curFormStructure);
    var records = dataAdapter.getRecordsHierarchy('id', 'parent_id', 'items', [{ name: 'name', map: 'value'}, {name: 'label', map: 'label'}]);
    $('#form_structure').jqxTree({ 
        source: records, 
        width: '95%', 
        height: '550px', 
        animationShowDuration: 500,
        hasThreeStates: true, 
        checkboxes: true
    });
    */
   
    $('#form_structure').jqxTreeGrid({ 
        width: '99%', 
        source: dataAdapter, 
        filterable: true,
        height: 550,
        checkboxes: true,
        hierarchicalCheckboxes: true,
        columns: [
          { text: 'Question Names', dataField: 'label', width: '100%', cellclassname: "no_border" }
        ],
    });
};

BadiliDash.prototype.initiateButtonsRadios = function(){
    $(".action_btn").on('click', dash.processButtonAction );
    $("#just_download, #download_save").on('click', dash.processDownloadChoice );

    $("#destination .custom").jqxRadioButton({ width: 250, height: 25});
    $("#destination .other").jqxRadioButton({ width: 250, height: 25});
};

BadiliDash.prototype.processRadioAction = function(){};

BadiliDash.prototype.getSelectedItems = function(node){
    $.each(node, function () {
        if (this.checked == true) {
            dash.selected_node_ids[dash.selected_node_ids.length] = this.name;
        }
        if (this.records != undefined){
            dash.getSelectedItems(this.records);
        }
    });
};

BadiliDash.prototype.processButtonAction = function(event){
    
    // save all checked items in selected_node_ids array.
    dash.selected_node_ids = new Array();

    // get all items.
    var top_level = $("#form_structure").jqxTreeGrid('getRows');
    dash.getSelectedItems(top_level);

    if(this.id == 'refresh_btn'){
        console.log('Refreshing the forms from the server');
    }
    else{
        if(dash.selected_node_ids === undefined){
            console.log('No forms defined...');
            swal({
              title: "Error!",
              text: "Please select at least one FORM to process.",
              imageUrl: "/static/img/error-icon.png"
            });
            return;
        }
        if(dash.selected_node_ids.length === 0){
            console.log('select nodes for processing...');
            swal({
              title: "Error!",
              text: "Please select at least one node for processing.",
              imageUrl: "/static/img/error-icon.png"
            });
            return;
        }
    }

    var action = undefined, data = undefined;
    dash.sel_form = $("#all_forms").jqxComboBox('getSelectedItem');

    switch(this.id){
        case 'get_data_btn':
            $('#confirmModal').modal('show');
            return;
        break;

        case 'update_btn':
            action = '/update_db_struct/';
        break;

        case 'refresh_btn':
            action = '/refresh_forms/';
        break;

        case 'delete_btn':
            action = '/delete_db/';
        break;
    };

    $('#spinnerModal').modal('show');
    $.ajax({
        type: "POST", url: action, dataType: 'json', data: data,
        error: dash.communicationError,
        success: function (data) {
            $('#spinnerModal').modal('hide');
            if (data.error) {
                Notification.show({create: true, hide: true, updateText: false, text: 'There was an error while communicating with the server', error: true});
                return;
            } else {
                if (action == '/refresh_forms/'){
                    dash.data.all_forms = data.all_forms;
                    dash.initiateAllForms();
                }
            }
        }
    });
};

/**
 * Process the download choice that the user has made
 * 
 * @return none
 */
BadiliDash.prototype.processDownloadChoice = function(){
    var view_name = undefined;
    if (this.id == 'download_save'){
        view_name = $('#view_name').val();
        if (view_name == '' || view_name == 'undefined'){
            $('#view_name-group').addClass('has-error');
            swal({
              title: "Error!",
              text: "Please enter the name of the view",
              imageUrl: "/static/img/error-icon.png"
            });
            return;
        }
    }
    // close the modal window
    $('#confirmModal').modal('hide');
    dash.downloadData(this.id, view_name);
};


/**
 * Initiate the download process for the data
 * 
 * @param {string}  action The action selected by the user
 * @param {string}  view_name   The name of the view that the user wants to save
 * @return {none}
 */
BadiliDash.prototype.downloadData = function(user_action, view_name){
    var action = '/get_data/';
    view_name = (view_name == undefined) ? '' : view_name;
    var data = {'nodes[]': dash.selected_node_ids, action: user_action, 'form_id': dash.sel_form.value, 'format': 'xlsx', 'view_name': view_name};

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        var a;
        if (xhttp.readyState === 4 && xhttp.status === 200) {
            // if it's an error, process it
            if(xhttp.response.type == 'text/json'){
                var reader = new FileReader();
                reader.onload = function() {
                    var response = JSON.parse(reader.result);
                    $('#spinnerModal').modal('hide');
                    swal({
                      title: "Error!",
                      text: response.message,
                      imageUrl: "/static/img/error-icon.png",
                      html: true
                    });
                }
                reader.readAsText(xhttp.response);
                return;
            }

            // Trick for making downloadable link
            a = document.createElement('a');
            a.href = window.URL.createObjectURL(xhttp.response);
            // Give filename you wish to download
            var d = new Date();

            var datestring =
                  d.getFullYear() + ("0"+(d.getMonth()+1)).slice(-2) + ("0" + d.getDate()).slice(-2)
                  + "_" +
                  ("0" + d.getHours()).slice(-2) + ("0" + d.getMinutes()).slice(-2) + ("0" + d.getSeconds()).slice(-2);

            a.download = 'Form'+ dash.sel_form.value + '_'+ datestring + '.xlsx';
            a.style.display = 'none';
            document.body.appendChild(a);
            $('#spinnerModal').modal('hide');
            a.click();
        }
    };

    $('#spinnerModal').modal('show');
    // Post data to URL which handles post request
    xhttp.open("POST", action);
    xhttp.setRequestHeader("X-CSRFToken", dash.csrftoken);
    xhttp.setRequestHeader("Content-Type", "application/json");

    // You should set responseType as blob for binary responses
    xhttp.responseType = 'blob';
    xhttp.send(JSON.stringify(data));
};

BadiliDash.prototype.fnFormatResult = function (value, searchString) {
    var pattern = '(' + searchString.replace(dash.reEscape, '\\$1') + ')';
    return value.value.replace(new RegExp(pattern, 'gi'), '<strong>$1<\/strong>');
};

BadiliDash.prototype.initiateDashHome = function(){
    dash.initiateIncidencesChart(dash.data.all_incidences.cattle, "cattle_incidences");
    dash.initiateIncidencesChart(dash.data.all_incidences.sheep, "sheep_incidences");
    dash.initiateIncidencesChart(dash.data.all_incidences.goats, "goat_incidences");
    dash.initiateIncidencesChart(dash.data.all_incidences.camels, "camel_incidences");
    dash.initiateWordCloud(dash.data.month_syndromes, 'month_syndromes');
    dash.initiateWordCloud(dash.data.week_syndromes, 'week_syndromes');

    // show the active reporting period
    $('.'+dash.r_period).addClass('active');
};

BadiliDash.prototype.initiateIncidencesChart = function(chart_data, div_id){
    var ctx = document.getElementById(div_id);
    var data = {
        labels: chart_data.labels,
        datasets: [{
            label: "Incidences",
            backgroundColor: dash.backgroundColor1[0],
            borderColor: dash.backgroundColor1[0],
            borderWidth: 3,
            data: chart_data.incidences,
            fill: false
        },
        {
            label: "Mortalities",
            backgroundColor: dash.backgroundColor1[1],
            borderColor: dash.backgroundColor1[1],
            borderWidth: 3,
            data: chart_data.fatalities,
            fill: false
        }]
    };
    var myLineChart = new Chart(ctx, {
        type: 'line',
        data: data,
        options: {}
    });  
};

BadiliDash.prototype.initiateWordCloud = function(cloud_data, div_id){
    dash.curD3div = div_id;
    d3.layout.cloud()
        .size([this.d3_width, this.d3_height])
        .words(cloud_data)
        // .rotate(function() {
        //     return (~~(Math.random() * 6) - 2.5) * 30;
        // })
        .rotate(0)
        .font("Impact")
        .fontSize(function(d) { return d.size; })
        .on("end", dash.drawD3Cloud)
        .start();

        var svg = document.getElementsByTagName("svg")[0];
        var bbox = svg.getBBox();
        var viewBox = [bbox.x, bbox.y, bbox.width, bbox.height].join(" ");
        svg.setAttribute("viewBox", viewBox);
};

BadiliDash.prototype.drawD3Cloud = function(words){
    var fill = d3.scale.category20();

    d3.select('#'+dash.curD3div).append("svg")
    .attr("width", this.d3_width)
    .attr("height", this.d3_height)
    .append("g")
    .attr("transform", "translate(" + ~~(this.d3_width / 2) + "," + ~~(this.d3_height / 2) + ")")
    .selectAll("text")
    .data(words)
    .enter().append("text")
    .style("font-size", function(d) { return d.size + "px"; })
    .style("-webkit-touch-callout", "none")
    .style("-webkit-user-select", "none")
    .style("-khtml-user-select", "none")
    .style("-moz-user-select", "none")
    .style("-ms-user-select", "none")
    .style("user-select", "none")
    .style("cursor", "default")
    .style("font-family", "Impact")
    .style("fill", function(d, i) { return fill(i); })
    .attr("text-anchor", "middle")
    .attr("transform", function(d) {
        return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
    })
    .text(function(d) {
        return d.text;
    });
};

BadiliDash.prototype.initiateSocialEconomicDash = function(){
    console.log('dashboard');
    dash.graphHHGender();
    dash.graphIncomeSources();
    dash.graphAvgIncomes();
};

/**
 * Create the graph for household head gender
 * @return {[type]} [description]
 */
BadiliDash.prototype.graphHHGender = function(){
    // formulate the data
    var countries = [], male = [], female = [];
    $.each(dash.data.hh_gender, function(country, that){
        countries[countries.length] = country;
        male[male.length] = (that.Male/that.sum)*100;
        female[female.length] = (that.Female/that.sum)*100;
    });

    var ctx = document.getElementById("hh_gender");
    var data = {
        labels: countries,
        datasets: [{
            label: "Male",
            backgroundColor: dash.backgroundColor1[0],
            borderColor: dash.backgroundColor1[0],
            borderWidth: 1,
            data: male,
            fill: false
        },
        {
            label: "Female",
            backgroundColor: dash.backgroundColor1[1],
            borderColor: dash.backgroundColor1[1],
            borderWidth: 1,
            data: female,
            fill: false
        }]
    };

    var hh_gender = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
            scales: {
                yAxes: [{stacked: true}]
            }
        }
    });
}

/**
 * Create a pie chart of the different income sources
 * @return {[type]} [description]
 */
BadiliDash.prototype.graphIncomeSources = function(){
    var ctx = document.getElementById("income_sources");
    var data = {
        labels: dash.data.incomes.i_types,
        datasets: [{
            backgroundColor: dash.backgroundColor2,
            hoverBackgroundColor: dash.backgroundColor2,
            data: dash.data.incomes.i_perc
        }]
    };

    var income_sources = new Chart(ctx, {
        type: 'pie',
        data: data,
        options: {
            animation:{
                animateScale:true
            }
        }
    });
}

/**
 * Draw the average incomes
 * @return {[type]} [description]
 */
BadiliDash.prototype.graphAvgIncomes = function(){
    console.log(dash.data);
    var ctx = document.getElementById("avg_incomes");
    var data = {
        labels: dash.data.avg_incomes.countries,
        datasets: [{
            label: "Income in USD",
            backgroundColor: dash.backgroundColor1[0],
            borderColor: dash.backgroundColor1[0],
            borderWidth: 1,
            data: dash.data.avg_incomes.avg
        }]
    };

    var avg_incomes = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {}
    });

    // incomes by gender
    var dtx = document.getElementById("income_by_gender");
    data = {
        labels: dash.data.avg_incomes_g.countries,
        datasets: [{
            label: "Male",
            backgroundColor: dash.backgroundColor1[0],
            borderColor: dash.backgroundColor1[0],
            borderWidth: 1,
            data: dash.data.avg_incomes_g.datasets.Male
        },
        {
            label: "Female",
            backgroundColor: dash.backgroundColor1[1],
            borderColor: dash.backgroundColor1[1],
            borderWidth: 1,
            data: dash.data.avg_incomes_g.datasets.Female
        }]
    };

    var income_by_gender = new Chart(dtx, {
        type: 'bar',
        data: data,
        options: {}
    });

    // land tenure system
    datasets = dash.createChartDatasets([dash.data.land_system.datasets], [dash.backgroundColor1]);
    etx = document.getElementById("land_systems");
    data = {
        labels: dash.data.land_system.countries,
        datasets: datasets
    };

    var land_system = new Chart(etx, {
        type: 'bar',
        data: data,
        options: {
            scales: {
                yAxes: [{stacked: true}]
            }
        }
    });

    ctx = document.getElementById("land_sizes");
    data = {
        labels: dash.data.land_sizes.countries,
        datasets: [{
            label: "Land sizes (in acres)",
            backgroundColor: dash.backgroundColor1[0],
            borderColor: dash.backgroundColor1[0],
            borderWidth: 1,
            data: dash.data.land_sizes.avg
        }]
    };

    var land_sizes = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {}
    });

    // who controls the income
    var datasets = dash.createChartDatasets([dash.data.income_control.datasets]);
    var etx = document.getElementById("income_control");
    data = {
        labels: dash.data.income_control.countries,
        datasets: datasets
    };

    var income_control = new Chart(etx, {
        type: 'bar',
        data: data,
        options: {}
    });
};

BadiliDash.prototype.initiateLivestockDash = function(){
    console.log(dash.data);
    dash.livestockDiversity();
    dash.breedsDiversity();
    dash.livestockOwnership();
    dash.livKeepingReasons();
};

BadiliDash.prototype.livestockDiversity = function(){
    // livestock diversity
    var datasets = dash.createChartDatasets([dash.data.liv_diversity.datasets], [dash.backgroundColor2]);
    var etx = document.getElementById("liv_diversity");
    var data = {
        labels: dash.data.liv_diversity.countries,
        datasets: datasets
    };

    var liv_diversity = new Chart(etx, {
        type: 'bar',
        data: data,
        options: {
            scales: {
                yAxes: [{stacked: true}]
            }
        }
    });
};

BadiliDash.prototype.breedsDiversity = function(){
    // create the structure that will accomodate the views
    var t_data = dash.data.breeds_diversity, div_prefix = 'b_div';
    var merged_views = dash.generateChartsHTMLTemplate(t_data, div_prefix)

    $('#breeds_per_country').after(merged_views);
    console.log("Finished creating the structure of the views");

    // lets initiate the graphs
    var i = 0;
    $.each(t_data, function(country, t_data){
        var view_id = sprintf("b_div_%s", country);

        var datasets = dash.createChartDatasets([t_data.datasets], [dash.backgroundColor1]);
        new Chart(document.getElementById(view_id), {
            type: 'bar',
            data: {
                labels: t_data.options,
                datasets: datasets
            },
            options: {
                scales: {
                    yAxes: [{stacked: true}]
                }
            }
        });
        i++;
    });
}

BadiliDash.prototype.livestockOwnership = function(){
    // create the structure that will accomodate the views
    var t_data = dash.data.liv_ownership, div_prefix = 'liv_own';
    var merged_views = dash.generateChartsHTMLTemplate(t_data, div_prefix);

    $('#liv_ownership').after(merged_views);
    console.log("Finished creating the structure of the livestock ownership views");

    // lets initiate the graphs
    var i = 0;
    $.each(t_data, function(country, t_data){
        var view_id = sprintf("%s_%s", div_prefix, country);

        var datasets = dash.createChartDatasets([t_data.datasets], [dash.backgroundColor1]);
        new Chart(document.getElementById(view_id), {
            type: 'bar',
            data: {
                labels: t_data.options,
                datasets: datasets
            },
            options: {
                scales: {
                    yAxes: [{stacked: true}]
                }
            }
        });
        i++;
    });
}

BadiliDash.prototype.livKeepingReasons = function(){
    // create the structure that will accomodate the views
    var t_data = dash.data.liv_keeping_reasons1, div_prefix = 'liv_keep';
    var t_data1 = dash.data.liv_keeping_reasons2;
    var merged_views = dash.generateChartsHTMLTemplate(t_data, div_prefix, dash.liv_keeping_template);

    $('#liv_keeping_reasons').after(merged_views);
    console.log("Finished creating the structure of the livestock ownership views");

    // lets initiate the graphs
    var i = 0;
    $.each(t_data, function(country, d_data){
        var view_id = sprintf("%s_%s", div_prefix, country);
        var stacked_data = [d_data.datasets, t_data1[country].datasets];
        var datasets = dash.createChartDatasets(stacked_data, dash.bgColors, 1);

        new Chart(document.getElementById(view_id), {
            type: 'bar',
            data: {
                labels: d_data.options,
                datasets: datasets
            },
            options: {
                scales: {
                    yAxes: [{stacked: true}]
                }
            }
        });
        i++;
    });
}

BadiliDash.prototype.initiateLivDash = function(){
    console.log(dash.data);
    // production systems
    var stacked_data = [
        dash.data.prod_systems_rainy.datasets,
        dash.data.prod_systems_dry.datasets
    ];
    var datasets = dash.createChartDatasets(stacked_data, dash.bgColors);

    new Chart(document.getElementById('prod_systems'), {
        type: 'bar',
        data: {
            labels: dash.data.prod_systems_rainy.top_l,
            datasets: datasets
        },
        options: {
            scales: {
                yAxes: [{stacked: true}]
            }
        }
    });

    // feeding systems
    var stacked_data = [
        dash.data.feed_systems_rainy.datasets,
        dash.data.feed_systems_dry.datasets
    ];
    var datasets = dash.createChartDatasets(stacked_data, dash.bgColors);

    new Chart(document.getElementById('feed_systems'), {
        type: 'bar',
        data: {
            labels: dash.data.prod_systems_rainy.top_l,
            datasets: datasets
        },
        options: {
            scales: {
                yAxes: [{stacked: true}]
            }
        }
    });

    var datasets = dash.createChartDatasets([dash.data.feed_types.datasets], dash.bgColors);
    new Chart(document.getElementById('feeds'), {
        type: 'bar',
        data: {
            labels: dash.data.feed_types.top_l,
            datasets: datasets
        },
        options: {
            scales: {
                yAxes: [{stacked: true}]
            }
        }
    });

    var datasets = dash.createChartDatasets([dash.data.tending_system.datasets], dash.bgColors);
    new Chart(document.getElementById('tending_mgmt'), {
        type: 'bar',
        data: {
            labels: dash.data.tending_system.top_l,
            datasets: datasets
        },
        options: {
            scales: {
                yAxes: [{stacked: true}]
            }
        }
    });
}

BadiliDash.prototype.createChartDatasets = function(d_data, color_scheme=[dash.backgroundColor2]){
    var datasets = new Array();

    $.each(d_data, function(i, t_data){
        var index = 0;
        $.each(t_data, function(title, data){
            datasets[datasets.length] = {
                label: title,
                backgroundColor: color_scheme[i][index],
                borderColor: color_scheme[i][index],
                borderWidth: 1,
                data: data,
                stack: i+1
            };
            index++;
        });
    });
    

    return datasets;
};

/**
 * Generate HTML section for creating the charts
 * 
 * @param  {[type]} t_data     [description]
 * @param  {[type]} div_prefix [description]
 * @return {[type]}            [description]
 */
BadiliDash.prototype.generateChartsHTMLTemplate = function(t_data, div_prefix, template=dash.default_template){
    var i = 0;
    $.each(t_data, function(country, s_data){
        var view_id = sprintf("%s_%s", div_prefix, country);
        var view = sprintf(template, country, view_id);
        if(i == 0){
            merged_views = "<div class='row'>" + view;
        }
        else if(i % 2 == 0){
            merged_views += "<div class='row'>";
            merged_views += view;
        }
        else{
            merged_views += view;
            merged_views += "</div>";
        }
        i++;
    });

    // if we had an even number, create a placeholder for closing the section
    if(i % 2 == 1){
        merged_views += "<div class='row'>&nbsp;</div>";
    }
    return merged_views;
}

BadiliDash.prototype.initiateViewsManagement = function(){
    // create the grid with the views data
    var $modal = $('#editor-modal'), $editor = $('#editor'), $editorTitle = $('#editor-title');
    var ft;
    var columns = [
        {'name': 'view_id', 'title': 'View ID'},
        {'name': 'view_name', 'title': 'View Name'},
        {'name': 'date_created', 'title': 'Date Created', 'type': 'date'},
        {'name': 'no_sub_tables', 'title': 'Sub Tables'},
        {'name': 'auto_process', 'title': 'Auto Process'},
        // {'name': 'structure', 'title': 'View Structure'}
    ];

    ft = FooTable.init('#views_table', {
        columns: columns,
        rows: dash.data.views,
        editing: {
            editRow: function(row){
                var values = row.val();
                $editor.find('#view_id').val(values.view_id);
                $editor.find('#view_name').val(values.view_name);
                $editor.find('#auto_process').val(values.auto_process);
                $modal.data('row', row);
                $editorTitle.text('Edit view ' + values.view_name);
                $modal.modal('show');
            },
            deleteRow: function(row){
                if (confirm('Are you sure you want to delete this view?')){
                    console.log('Deleting a row');
                    data = {'view_id': row.value.view_id}
                    $('#spinnermModal').modal('show');
                    $.ajax({
                        type: "POST", url: "/delete_view/", dataType: 'json', data: {'view': JSON.stringify(data)},
                        error: dash.communicationError,
                        success: function (data) {
                            $('#spinnermModal').modal('hide');
                            if (data.error) {
                                console.log(data.message);
                                swal({
                                  title: "Error!",
                                  text: data.message,
                                  imageUrl: "/static/img/error-icon.png"
                                });
                                return;
                            }
                            else {
                                // all is ok, so delete the row on the interface
                                row.delete();
                                swal({
                                  title: "Success",
                                  text: "The view has been deleted successfully",
                                  imageUrl: "/static/img/success-icon.png"
                                });
                            }
                        }
                    });
                }
            }
        }
    }),
    uid = 10001;

    $editor.on('submit', dash.submitViewEdits);
};

BadiliDash.prototype.submitViewEdits = function(e){
    var $modal = $('#editor-modal'), $editor = $('#editor'), $editorTitle = $('#editor-title');
    if (this.checkValidity && !this.checkValidity()){
        return;
    }
    console.log('Submiting edits');
    e.preventDefault();
    var row = $modal.data('row'),
        values = {
            view_id: $editor.find('#view_id').val(),
            view_name: $editor.find('#view_name').val(),
            auto_process: $editor.find('#auto_process').val()
        };

    $('#spinnermModal').modal('show');
    $.ajax({
        type: "POST", url: "/edit_view/", dataType: 'json', data: {'view': JSON.stringify(values)},
        error: dash.communicationError,
        success: function (data) {
            $('#spinnermModal').modal('hide');
            if (data.error) {
                console.log(data.message);
                swal({
                  title: "Error!",
                  text: data.message,
                  imageUrl: "/static/img/error-icon.png"
                });
                return;
            }
            else {
                // all is ok, modify the data on the table view
                if (row instanceof FooTable.Row){
                    row.val(values);
                } else {
                    values.id = uid++;
                    ft.rows.add(values);
                }
            }
        }
    });
    
    $modal.modal('hide');
};

BadiliDash.prototype.initiateIncidentsMap = function(){
    console.log("Initiating maps...");
    var testData = {
      // max: 8,
      data: dash.data.locations
    };
    var baseLayer = L.tileLayer(
      'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
        attribution: ' <a href="https://badili.co.ke">Badili Innovations</a>',
        maxZoom: 18
      }
    );

    var cfg = {
      // radius should be small ONLY if scaleRadius is true (or small radius is intended)
      // if scaleRadius is false it will be the constant radius used in pixels
      "radius": 0.07,
      "maxOpacity": .8, 
      // scales the radius based on map zoom
      "scaleRadius": true, 
      // if set to false the heatmap uses the global maximum for colorization
      // if activated: uses the data maximum within the current map boundaries 
      //   (there will always be a red spot with useLocalExtremas true)
      "useLocalExtrema": false,
      // which field name in your data represents the latitude - default "lat"
      latField: 'lat',
      // which field name in your data represents the longitude - default "lng"
      lngField: 'lng',
      // which field name in your data represents the data value - default "value"
      valueField: 'count'
    };

    var heatmapLayer = new HeatmapOverlay(cfg);
    var map = new L.Map('reporting_map', {
      center: new L.LatLng(dash.data.center_point.lat, dash.data.center_point.lng),
      zoom: 8,
      layers: [baseLayer, heatmapLayer]
    });

    heatmapLayer.setData(testData);
};

BadiliDash.prototype.morbidity_mortality = function(){
    var ctx = document.getElementById("morbidity_mortality");
    var datasets = new Array();
    dash.mm_real_data = [];
    dash.mm_real_data1 = [];
    $.each(dash.data.all_species, function(i, specie){
        var sick = [], dead = []
        $.each(dash.data.sub_counties, function(j, sub_county){
            sp_data = dash.data.by_species[sub_county][specie]
            sick[sick.length] = sp_data.sick;
            dead[dead.length] = sp_data.dead;
            dash.mm_real_data[sp_data.sick+''+(sick.length-1)] = sp_data.sick_r;
            dash.mm_real_data[sp_data.dead+''+(dead.length-1)] = sp_data.dead_r;
            
        });
        datasets[datasets.length] = {
            label: specie+' (Morbidity)',
            backgroundColor: dash.backgroundColor1[i],
            borderColor: dash.backgroundColor1[i],
            borderWidth: 1,
            data: sick,
            stack: i+1
        };
        datasets[datasets.length] = {
            label: specie+' (Mortality)',
            backgroundColor: dash.backgroundColor2[i],
            borderColor: dash.backgroundColor2[i],
            borderWidth: 1,
            data: dead,
            stack: i+1
        };
    });

    new Chart(document.getElementById("morbidity_mortality"), {
        type: 'bar',
        data: {
            labels: dash.data.sub_counties,
            datasets: datasets
        },
        options: {
            scales: {
                // yAxes: [{stacked: true}],
                xAxes: [{stacked: true}],
                yAxes: [{scaleLabel: {
                    display: true,
                    labelString: 'Log [e, cases]'
                    }
                }]
            },
            animation: {
                duration: 1,
                onComplete: function () {
                    var chartInstance = this.chart, ctx = chartInstance.ctx;
                    ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'bottom';

                    this.data.datasets.forEach(function (dataset, j) {
                        // console.log(dash.mm_real_data);
                        for (var i = 0; i < dataset.data.length; i++) {
                            console.log(i+' - '+j);
                            if(dataset.data[i] == 0){
                                continue;
                            }
                            var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                            ctx.fillText(dash.mm_real_data[dataset.data[i]+''+i]+' cases', model.x, model.y - 2);
                        }
                    });
                }
            }
        }
    });
}

/**
 * Show a notification on the page
 *
 * @param   message     The message to be shown
 * @param   type        The type of message
 */
BadiliDash.prototype.showNotification = function(message, type, autoclose){
   if(type === undefined) { type = 'error'; }
   if(autoclose === undefined) { autoclose = true; }

   $('#messageNotification div').html(message);
   if($('#messageNotification').jqxNotification('width') === undefined){
      $('#messageNotification').jqxNotification({
         width: 350, position: 'top-right', opacity: 0.9,
         autoOpen: false, animationOpenDelay: 800, autoClose: autoclose, template: type
       });
   }
   else{ $('#messageNotification').jqxNotification({template: type}); }

   $('#messageNotification').jqxNotification('open');
};

BadiliDash.prototype.communicationError = function(){
    $('#spinnerModal').modal('hide');
};

BadiliDash.prototype.changeReportingPeriod = function(){
    var r_period = $(this).data('period');
    window.location.href = '/dashboard?r_period='+r_period;
};

BadiliDash.prototype.initiateBiweeklyReport = function(){
    // change the default height and width
    dash.d3_width = 450;
    dash.d3_height = 300;
    dash.initiateWordCloud(dash.data.small_ruminant_diseases, 'small_ruminants_diseases');
    dash.initiateWordCloud(dash.data.cattle_diseases, 'cattle_diseases');
    dash.initiateWordCloud(dash.data.small_ruminant_syndromes, 'small_ruminants_syndromes');
    dash.initiateWordCloud(dash.data.cattle_syndromes, 'cattle_syndromes');
    dash.morbidity_mortality();
};

var dash = new BadiliDash();
