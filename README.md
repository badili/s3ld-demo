## S3LD Turkana Syndromic Surveillance System

### Hosting on Docker Containers

- Clone onadata repo

        cd /opt
        git clone git@gitlab.com:badili/s3ld_demo.git
        cd s3ld_demo

- login to [gitlab docker register](https://gitlab.com/help/user/project/container_registry)

        docker login registry.gitlab.com

- Build all containers

        docker-compose build

- start all containers; run in background(`-d`)

        docker-compose up -d

- access s3ld app on the brower `http://localhost:9012/`

- proxy-passing from nginx on the host to your docker-compose stack

        upstream s3ld-demo {
            server localhost:9012;
        }
        server {
            listen       80;
            server_name  s3ld-demo.badili.co.ke;
            client_max_body_size 50m;
            error_log /var/log/nginx/s3ld-demo.badili.go.ke_error.log warn;
            access_log /var/log/nginx/s3ld-demo.badili.co.ke_access.log combined;

            location / {
                include /etc/nginx/proxy_params;
                proxy_pass http://s3ld-demo;
            }
        }
