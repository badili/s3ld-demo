"""
Django settings for demo project.

Generated by 'django-admin startproject' using Django 1.10.5.

For more information on this file, see
https://docs.djangoproject.com/en/1.10/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.10/ref/settings/
"""

import os

# SITE_ROOT = os.path.dirname(os.path.realpath(__file__))
# STATICFILES_DIRS = (
#     os.path.join(SITE_ROOT, '/opt/s3ld_demo/static/'),
# )

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/

# STATIC_URL = 'static/'
# STATIC_ROOT = '/opt/s3ld_demo/demo/static/'
# WKHTMLTOPDF_CMD = '/usr/local/bin/wkhtmltopdf'

BASE_DIR = os.path.dirname(__file__)
PROJECT_DIR = os.path.dirname(BASE_DIR)
PROJECT_ROOT = os.path.dirname(os.path.dirname(__file__))
STATIC_URL = '/static/'
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
    '/www/django_static/',
]
# STATIC_ROOT = os.path.join(PROJECT_DIR, 'static')

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.10/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'xw9xyc$nyym$q0-3-pozdek-f0o_z1xpktpm8ex36k9g&0464v'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ["*"]

DEFAULT_PORT = 8089

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_crontab',
    'livereload',

    # demo
    'demo'
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'livereload.middleware.LiveReloadScript',
]

ROOT_URLCONF = 'demo.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.jinja2.Jinja2',
        'DIRS': [os.path.join(BASE_DIR, 'templates/jinja2')],
        'APP_DIRS': True,
        'OPTIONS': {'environment': 'demo.jinja2_settings.environment',},
    },
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates/django')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'demo.wsgi.application'

SITE_NAME = 'Demo S3LD'

DEFAULT_REPORTING_PERIOD = 'past_week'

SESSION_ENGINE = 'django.contrib.sessions.backends.signed_cookies'

# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': os.environ.get('DEFAULT_DB_ENGINE', 'django.db.backends.postgresql_psycopg2'),
        'NAME': os.environ.get('DEFAULT_DB_NAME', 's3ld_demo'),
        'USER': os.environ.get('DEFAULT_DB_USER', 'onadata'),
        'PASSWORD': os.environ.get('DEFAULT_DB_PASS', 'pRbV5CJ9ed'),
        'HOST': os.environ.get('DEFAULT_DB_HOST', 'localhost'),
        'PORT': os.environ.get('DEFAULT_DB_PORT', 5432)
    }
}

ONADATA_SETTINGS = {
    'HOST': os.environ.get('ONADATA_URL', 'http://onadata.badili.co.ke/'),
    'USER': os.environ.get('ONADATA_USER', 'training'),
    'PASSWORD': os.environ.get('ONADATA_PASSWORD', 'Training2018'),
    # 'API_TOKEN': os.environ.get('ONADATA_API_TOKEN', '37f92a721c063ce49416c481e0da5584d15da914'),
    'API_TOKEN': os.environ.get('ONADATA_API_TOKEN', 'ddc3109b5fa86db10f0160f621c25672f6a7a139'),        # Training onadata token
}


CRONJOBS = [
    ('*/5 * * * *', 'demo.odk_forms.auto_process_submissions', '>> /tmp/demo_cron.log')
]


# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Africa/Nairobi'

USE_I18N = True

USE_L10N = True

USE_TZ = False
