from __future__ import absolute_import  # Python 2 only

import json
import logging
import traceback
import datetime

import posixpath
from urlparse import unquote

from django.conf import settings
from django.contrib.staticfiles import finders
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.views import static
from django.http import HttpResponse, Http404
from django.shortcuts import render, redirect
from django.middleware import csrf

from wsgiref.util import FileWrapper

from .odk_forms import OdkForms
from .terminal_output import Terminal

import os
terminal = Terminal()


def login_page(request):
    csrf_token = get_or_create_csrf_token(request)
    page_settings = {'page_title': "%s | Login Page" % settings.SITE_NAME, 'csrf_token': csrf_token}
    terminal.tprint(csrf_token, 'ok')

    try:
        username = request.POST['username']
        password = request.POST['pass']

        if username is not None:
            user = authenticate(username=username, password=password)

            if user is None:
                terminal.tprint("Couldn't authenticate the user... redirect to login page", 'fail')
                page_settings['error'] = 'Invalid username or password'
                return render(request, 'login.html', page_settings)
            else:
                terminal.tprint('All ok', 'debug')
                login(request, user)
                return redirect('/dashboard', request=request)
        else:
            return render(request, 'login.html')
    except KeyError as e:
        # ask the user to enter the username and/or password
        terminal.tprint('Username/password not defined', 'warn')
        page_settings['message'] = "Please enter your username and password"
        return render(request, 'login.html', page_settings)
    except Exception as e:
        terminal.tprint(str(e), 'fail')
        page_settings['error'] = "There was an error while authenticating.<br />Please try again and if the error persist, please contact the system administrator"
        return render(request, 'login.html', page_settings)


def logout_view(request):
    logout(request)
    csrf_token = get_or_create_csrf_token(request)
    return render(request, 'landing.html')


def under_review_page(request):
    csrf_token = get_or_create_csrf_token(request)
    return render(request, 'under_review.html')


def landing_page(request):
    csrf_token = get_or_create_csrf_token(request)
    return render(request, 'landing.html')


@login_required(login_url='/login')
def download_page(request):
    csrf_token = get_or_create_csrf_token(request)

    # get all the data to be used to construct the tree
    odk = OdkForms(request)
    all_forms = odk.get_all_forms()
    page_settings = {
        'page_title': "%s | Downloads" % settings.SITE_NAME,
        'csrf_token': csrf_token,
        'section_title': 'Download Section',
        'all_forms': json.dumps(all_forms)
    }
    return render(request, 'download.html', page_settings)


@login_required(login_url='/login')
def modify_view(request):

    odk = OdkForms(request)
    if (request.get_full_path() == '/edit_view/'):
        response = odk.edit_view(request)
    elif (request.get_full_path() == '/delete_view/'):
        response = odk.delete_view(request)

    return HttpResponse(json.dumps(response))


@login_required(login_url='/login')
def manage_views(request):
    csrf_token = get_or_create_csrf_token(request)

    # get all the data to be used to construct the tree
    odk = OdkForms(request)
    all_data = odk.get_views_info()

    page_settings = {
        'page_title': "%s | Manage Generated Views" % settings.SITE_NAME,
        'csrf_token': csrf_token,
        'section_title': 'Manage Views',
        'all_data': json.dumps(all_data)
    }
    return render(request, 'manage_views.html', page_settings)


@login_required(login_url='/login')
def update_db(request):
    odk = OdkForms(request)

    try:
        odk.update_sdss_db()
    except Exception as e:
        logging.error(traceback.format_exc())
        print str(e)
        return HttpResponse(traceback.format_exc())

    return HttpResponse(json.dumps({'error': False, 'message': 'Database updated'}))


@login_required(login_url='/login')
def show_dashboard(request):
    csrf_token = get_or_create_csrf_token(request)

    odk = OdkForms(request)
    stats = odk.system_stats()
    r_period = request.session['r_period']

    if r_period == 'past_week':
        period_narrative = 'For the previous 7 days'
    elif r_period == 'past_month':
        period_narrative = 'For the past month'
    elif r_period == 'past_3mo':
        period_narrative = 'For the past 3 months'
    elif r_period == 'past_6mo':
        period_narrative = 'For the past 6 months'
    else:
        period_narrative = 'Default: Last 7 days'

    page_settings = {
        'page_title': "%s | Home" % settings.SITE_NAME,
        'csrf_token': csrf_token,
        'section_title': 'Turkana SDSS Overview',
        'data': stats,
        'r_period': r_period,
        'period_narrative': period_narrative
    }
    return render(request, 'dash_home.html', page_settings)


@login_required(login_url='/login')
def form_structure(request):
    # given a form id, get the structure for the form
    odk = OdkForms(request)
    try:
        form_id = int(request.POST['form_id'])
        structure = odk.get_form_structure_as_json(form_id)
    except KeyError:
        return HttpResponse(traceback.format_exc())
    except Exception as e:
        print (str(e))
        logging.error(traceback.format_exc())

    return HttpResponse(json.dumps({'error': False, 'structure': structure}))


@login_required(login_url='/login')
def form_structure(request):
    # given a form id, get the structure for the form
    odk = OdkForms(request)
    try:
        form_id = int(request.POST['form_id'])
        structure = odk.get_form_structure_as_json(form_id)
    except KeyError as e:
        logging.error(traceback.format_exc())
        return HttpResponse(json.dumps({'error': True, 'message': str(e)}))
    except Exception as e:
        logging.info(str(e))
        logging.debug(traceback.format_exc())
        return HttpResponse(json.dumps({'error': True, 'message': str(e)}))

    return HttpResponse(json.dumps({'error': False, 'structure': structure}))


@login_required(login_url='/login')
def download_data(request):
    # given the nodes, download the associated data
    odk = OdkForms(request)
    try:
        data = json.loads(request.body)
        res = odk.fetch_merge_data(data['form_id'], data['nodes[]'], data['format'], data['action'], data['view_name'])
    except KeyError as e:
        response = HttpResponse(json.dumps({'error': True, 'message': str(e)}), content_type='text/json')
        response['Content-Message'] = json.dumps({'error': True, 'message': str(e)})
        return response
    except Exception as e:
        logging.debug(traceback.format_exc())
        logging.error(str(e))
        response = HttpResponse(json.dumps({'error': True, 'message': str(e)}), content_type='text/json')
        response['Content-Message'] = json.dumps({'error': True, 'message': str(e)})
        return response

    if res['is_downloadable'] is True:
        filename = res['filename']
        wrapper = FileWrapper(file(filename))
        response = HttpResponse(wrapper, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        response['Content-Disposition'] = 'attachment; filename=%s' % os.path.basename(filename)
        response['Content-Length'] = os.path.getsize(filename)
    else:
        response = HttpResponse(json.dumps({'error': False, 'message': res['message']}), content_type='text/json')
        response['Content-Message'] = json.dumps({'error': False, 'message': res['message']})

    return response


@login_required(login_url='/login')
def download(request):
    # given the nodes, download the associated data
    odk = OdkForms(request)
    try:
        data = json.loads(request.body)
        filename = odk.fetch_data(data['form_id'], data['nodes[]'], data['format'])
    except KeyError:
        return HttpResponse(traceback.format_exc())
    except Exception as e:
        print str(e)
        logging.error(traceback.format_exc())

    wrapper = FileWrapper(file(filename))
    response = HttpResponse(wrapper, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename=%s' % os.path.basename(filename)
    response['Content-Length'] = os.path.getsize(filename)

    return response


@login_required(login_url='/login')
def refresh_forms(request):
    """
    Refresh the database with any new forms
    """
    odk = OdkForms(request)

    try:
        all_forms = odk.refresh_forms()
    except Exception:
        logging.error(traceback.format_exc())

    return HttpResponse(json.dumps({'error': False, 'all_forms': all_forms}))


def get_or_create_csrf_token(request):
    token = request.META.get('CSRF_COOKIE', None)
    if token is None:
        token = csrf._get_new_csrf_string()
        request.META['CSRF_COOKIE'] = token
    request.META['CSRF_COOKIE_USED'] = True
    return token


def serve_static_files(request, path, insecure=False, **kwargs):
    """
    Serve static files below a given point in the directory structure or
    from locations inferred from the staticfiles finders.
    To use, put a URL pattern such as::
        from django.contrib.staticfiles import views
        url(r'^(?P<path>.*)$', views.serve)
    in your URLconf.
    It uses the django.views.static.serve() view to serve the found files.
    """

    if not settings.DEBUG and not insecure:
        raise Http404
    normalized_path = posixpath.normpath(unquote(path)).lstrip('/')
    absolute_path = finders.find(normalized_path)
    if not absolute_path:
        if path.endswith('/') or path == '':
            raise Http404("Directory indexes are not allowed here.")
        raise Http404("'%s' could not be found" % path)
    document_root, path = os.path.split(absolute_path)
    return static.serve(request, path, document_root=document_root, **kwargs)


def biweekly(request):
    csrf_token = get_or_create_csrf_token(request)

    # get all the data to be used to construct the tree
    odk = OdkForms(request)
    all_data = odk.get_biweekly_report()
    terminal.tprint(json.dumps(all_data['sc']), 'ok')
    today = datetime.date.today()
    today_f = today.strftime('%a, %d %b %Y')

    page_settings = {
        'page_title': "%s | Biweekly Report" % settings.SITE_NAME,
        'csrf_token': csrf_token,
        'section_title': 'Biweekly Report',
        'data': all_data,
        'today_f': today_f,
        'report_no': all_data['report_no']
    }
    # PDFTemplateResponse(request=request, context=page_settings, template='biweekly_report_pdf.html', filename='biweekly_report.pdf')
    return render(request, 'biweekly_report_pdf.html', page_settings)
